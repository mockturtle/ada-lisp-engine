pragma Ada_2012;
with Ada.Tags;

package body Embedded_LISP is

   -------------
   -- Type_Of --
   -------------

   function Type_Of (Item : Abstract_LISP_Value'Class) return LISP_Type
   is
      use Ada.Tags;

      Type_To_Tag : constant array (Builtin_Type) of Ada.Tags.Tag
        := (Int    => LISP_Integer'Tag,
            Real   => LISP_Float'Tag,
            Text   => LISP_String'Tag,
            Symbol => LISP_Symbol'Tag,
            List   => LISP_List'Tag);
   begin
      for type_name in Builtin_Type loop
         if Type_To_Tag (type_name) = Item'Tag then
            return Type_Name;
         end if;
      end loop;

      return User_Defined;
   end Type_Of;


   ------------
   -- Append --
   ------------

   procedure Append (To : in out LISP_List; Item : LISP_Value) is
   begin
      To.Value.Append (Item);
   end Append;


   ---------
   -- Car --
   ---------

   function Car (Item : LISP_List) return Lisp_Value is
   begin
      pragma Compile_Time_Warning (Standard.True, "Car unimplemented");
      return raise Program_Error with "Unimplemented function Car";
   end Car;

   ---------
   -- Cdr --
   ---------

   function Cdr (Item : LISP_List) return LISP_List is
   begin
      pragma Compile_Time_Warning (Standard.True, "Cdr unimplemented");
      return raise Program_Error with "Unimplemented function Cdr";
   end Cdr;

   ---------
   -- Nth --
   ---------

   function Nth (Item : LISP_List; Index : Natural) return Lisp_Value is
   begin
      pragma Compile_Time_Warning (Standard.True, "Nth unimplemented");
      return raise Program_Error with "Unimplemented function Nth";
   end Nth;

end Embedded_LISP;

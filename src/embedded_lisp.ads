with Ada.Containers.Indefinite_Vectors;
with Ada.Strings.Unbounded;

package Embedded_LISP is
   type Abstract_LISP_Value is interface;
   
   subtype Lisp_Value is Abstract_LISP_Value'Class; 

   type LISP_Type is (Int, Real, Text, Symbol, List, User_Defined);
   subtype Builtin_Type is LISP_Type range  Int .. List;
   
   function Type_Of (Item : Abstract_LISP_Value'Class) return LISP_Type;

   -- LISP Integers --
   -------------------
   
   type LISP_Integer is new Abstract_LISP_Value with private;
   
   function Value (Item : LISP_Integer) return Integer;
   
   function To_Lisp (Item : Integer) return LISP_Integer;
   
   -- LISP Floats --
   -------------------
   type LISP_Float   is new Abstract_LISP_Value with private;
   
   function Value (Item : LISP_Float) return Float;
   
   function To_Lisp (Item : Float) return LISP_Float;
   
   -- LISP strings --
   ------------------
   type LISP_String  is new Abstract_LISP_Value with private;

   function Value (Item : LISP_String) return String;
   
   function To_Lisp (Item : String) return LISP_String;
   
   
   -- LISP strings --
   ------------------
   
   type LISP_Symbol  is new Abstract_LISP_Value with private;

   function Name (Item : LISP_Symbol) return String;
   
   function To_Symbol (Name : String) return LISP_Symbol;

   -- LISP strings --
   ------------------
   type LISP_List    is new Abstract_LISP_Value with private;
   
   Nil : constant LISP_List;
   
   procedure Append (To : in out LISP_List;
                     Item : LISP_Value);
   
   function Length (Item : LISP_List) return Natural;

   function Is_Empty (Item : LISP_List) return Boolean;
   
   function Car (Item : LISP_List) return Lisp_Value;
   function Cdr (Item : LISP_List) return LISP_List;
   
   function Nth (Item : LISP_List; Index : Natural) return Lisp_Value;

private
   use Ada.Strings.Unbounded;
   
   package LISP_Value_Lists is 
     new Ada.Containers.Indefinite_Vectors
       (Index_Type   => Natural,
        Element_Type => Abstract_LISP_Value'Class);
   
   type LISP_Integer is new Abstract_LISP_Value 
   with 
      record
         Value : Integer;
      end record;
   
   function Value (Item : LISP_Integer) return Integer
   is (Item.Value);
   
   function To_Lisp (Item : Integer) return LISP_Integer
   is (LISP_Integer'(Value => Item));

   
   type LISP_Float   is new Abstract_LISP_Value  
   with 
      record
         Value : Float;
      end record;

   function Value (Item : LISP_Float) return Float
   is (Item.Value);
   
   function To_Lisp (Item : Float) return LISP_Float
   is (LISP_Float'(Value => Item));

   type LISP_String  is new Abstract_LISP_Value 
   with 
      record
         Value : Unbounded_String;
      end record;

   function Value (Item : LISP_String) return String
   is (To_String(Item.Value));
   
   function To_Lisp (Item : String) return LISP_String
   is (LISP_String'(Value => To_Unbounded_String(Item)));


   type LISP_Symbol  is new Abstract_LISP_Value  
   with 
      record
         Name : Unbounded_String;
      end record;

   function Name (Item : LISP_Symbol) return String
   is (To_String(Item.name));
   
   function To_Symbol (name : String) return LISP_Symbol
   is (LISP_Symbol'(name => To_Unbounded_String(name)));


     
   type LISP_List    is new Abstract_LISP_Value 
   with
      record 
         Value : Lisp_Value_Lists.Vector;
      end record;

   
   Nil : constant LISP_List := 
           LISP_List'(Value => LISP_Value_Lists.Empty_Vector);
   
   function Length (Item : LISP_List) return Natural 
   is (Natural (Item.Value.Length));
   
   function Is_Empty (Item : LISP_List) return Boolean
   is (Length (Item) = 0);
   
   function Car (Item : LISP_List) return Lisp_Value 
   is (if Item.Is_Empty then
          Nil
       else
          Item.Value.First_Element);

   

end Embedded_LISP;

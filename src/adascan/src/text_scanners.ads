package Text_Scanners is

   Unexpected_EOF     : exception;
   Unrecognized_Token : exception;
   Unmatched_Token    : exception;

end Text_Scanners;

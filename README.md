# What is this?

This project provides a simple package that allows to embed a small LISP interpreter in a program.  I wrote it because I needed to add fairly complex configuration capabilities to a program of mine.

# How do I use it?
## Installation
Just put the sources where your compiler will find them.

## The language

This is the syntax of the language

```abnf
   program = 1*(value LWSP)
   value = [quote] symbol / number / string / [quote] list
   quote = "'"
   number = integer /  float
   list   = "(" [program] ")"
   symbol = ALPHA *VCHAR / single-char-symbol
   single-char-symbol = "+" / "/" / "-" / ":" / "*"
```

Comments start from a ";" and end at the end of line
Note that there is no "dotted pair," in my experience the needs of using dotted pairs like `(a . b)` are quite uncommon, therefore I decided to keep them out to simplify.

## Evaluation 

* The evaluation of an integer, a float and a string is the value itself
* In the evaluation of a symbol the symbol is interpreted as a variable
* In the evaluation of a list
  * The first element must be a symbol that must have a function associated with
  * The other elements of the list are used as parameters **unevaluated**.
  * The result of the function is the result of the evaluation of the list
* Finally, the evaluation of a program gives as result the value obtained by the evaluation of the last element of the program
* A quoted symbol or list is transformed into the equivalent 

```lisp 
(quote something)
```

### LISP values in Ada 

In Ada a LISP value is represented by a descendant of 

```ada
   type Abstract_LISP_Value is interface;
   type LISP_Value is access all Abstract_LISP_Value'class;
```
This allows the user of the library to define new types that can be obtained as result of built-in functions. 

An enumerative type is defined to simplify flow control on the basis of the actual value type.

```ada
   type LISP_type is (symbol, int, real, text, list, user_defined);

   function type_of(Value : Abstract_LISP_Value'class) return LISP_type;
```

## Structure of a LISP interpreter

The LISP interpreter is represented by type
```ada
   type LISP_Interpreter is private;
```

A LISP interpreter has internally a *variable/function map*  that associate, respectively, symbols to LISP values and function descriptors. 

* The interpreter map can be stacked in order to implement local variables.
* The interpreter map can be dumped/loaded, allowing for saving the current state of the interpreter.  Builtin functions are not saved since it is not clear if reloading would make any sense.

Procedures are provided to read/write the tables

```ada
    procedure Write_Variable(interp : in out LISP_Interpreter;
                             name   :        LISP_Symbol;
                             value  :        LISP_Value);

    procedure Define_Builtin (interp   : in out LISP_Interpreter;
                              name     :        LISP_Symbol;
                              callback :        Builtin_Callback);

    procedure Define_LISP_Function (interp : in out LISP_Interpreter;
                                    name   :        LISP_Symbol;
                                    specs  :        LISP_List;
                                    code   :        LISP_Program);

    function is_variable (interp : LISP_Interpreter;
                          name   : LISP_Symbol)
                          return boolean;

    function value_of (interp : LISP_Interpreter;
                       name   : LISP_Symbol)
                       return LISP_Value
      with pre => is_variable(interp, name);
                        

    function is_function (interp : LISP_Interpreter;
                          name   : LISP_Symbol)
                          return boolean;
```

Interpretation happens in two steps
* First the program is compiled into an internal form 
* Then the internal form is interpreted
* A "shortcut" function that does the two steps together will be provided 

A compiled program is represented by the type
```ada
   type LISP_Program is private;

   function parse (interp : LISP_Interpreter;
                   source : string)
                   return LISP_Program;

   function Run(interp : in out LISP_Interpreter;
                code   : LISP_Program)
                return Abstract_LISP_Value'class;                
```

A builtin function is implemented as a callback
```ada
   type Builtin_Callback is 
       access function (interp     : in out LISP_Interpreter;
                        parameters : LISP_Value_Vector)
                        return Abstract_LISP_Value'class;
```   

where `Parameters` contains the parameters given to the function **unevaluated**. It is the duty of the callback to evaluate them, if necessary.  Some helping functions are provided 

```ada
   function eval(interp: LISP_Interpreter;   
                 item : LISP_Value_Vector) 
                 return LISP_Value_Vector;
```
This function evaluates every element of `Item` and return a vector of the results.  Other functions able to handle default values and/or optional parameters will be provided.